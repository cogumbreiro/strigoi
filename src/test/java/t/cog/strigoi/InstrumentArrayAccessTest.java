package t.cog.strigoi;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Vector;

import static org.junit.Assert.*;

public class InstrumentArrayAccessTest {
    private static boolean hasGet = false;
    private static boolean hasPut = false;
    private static boolean hasInit = false;
    private static Vector<Integer> lastAccess = new Vector<>();

    public static void initError() {
        hasInit = true;
    }

    public static void initArray(Object arr) {
        hasInit = true;
    }

    public static void accessArray(Object arr, int idx) {
        lastAccess.add(idx);
    }

    public static void accessArray(Object arr, int idx, boolean isPut) {
        hasPut = isPut;
        hasGet = ! isPut;
        lastAccess.add(idx);
    }

    @Before
    public void init() {
        hasGet = false;
        hasPut = false;
        hasInit = false;
        lastAccess = new Vector<>();
    }

    @Test
    public void checkError() throws NoSuchMethodException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException {
        try {
            ClassLoader cl = InstrumentArrayAccess.make()
                    .onAccess(InstrumentArrayAccessTest.class, "accessArray")
                    .onInit(InstrumentArrayAccessTest.class, "initError") // <- error
                    .buildClassLoader();
            fail();
        } catch (NoSuchMethodException e) {
            // OK
        }
    }

    @Test
    public void empty() throws NoSuchMethodException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException {
        ClassLoader cl = InstrumentArrayAccess.make()
                .onAccess(InstrumentArrayAccessTest.class, "accessArray")
                .onInit(InstrumentArrayAccessTest.class, "initArray")
                .buildClassLoader();
        Class<?> cls = cl.loadClass("t.cog.strigoi.Foo");
        cls.newInstance();
    }

    @Test
    public void checkGetterSetterL() throws NoSuchMethodException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException, InvocationTargetException {
        ClassLoader cl = InstrumentArrayAccess.make()
                        .onInit(InstrumentArrayAccessTest.class, "initArray")
                        .onAccess(InstrumentArrayAccessTest.class, "accessArray")
                        .buildClassLoader("t.cog.strigoi.Foo"::equals);
        Class<?> cls = cl.loadClass("t.cog.strigoi.Foo");
        Method run = cls.getMethod("handleArrayL", new Class[]{int.class});
        Object obj = cls.newInstance();
        run.invoke(obj, new Object[]{20});
        assertTrue(hasInit);
        assertEquals(Arrays.asList(10, 20 - 3), lastAccess);
    }

    @Test
    public void checkGetterSetterI() throws NoSuchMethodException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException, InvocationTargetException {
        ClassLoader cl = InstrumentArrayAccess.make()
                .onInit(InstrumentArrayAccessTest.class, "initArray")
                .onAccess(InstrumentArrayAccessTest.class, "accessArray")
                .buildClassLoader("t.cog.strigoi.Foo"::equals);
        Class<?> cls = cl.loadClass("t.cog.strigoi.Foo");
        Method run = cls.getMethod("handleArrayI", new Class[]{int.class});
        Object obj = cls.newInstance();
        run.invoke(obj, new Object[]{20});
        assertTrue(hasInit);
        assertEquals(Arrays.asList(10, 20 - 3), lastAccess);
    }


}
