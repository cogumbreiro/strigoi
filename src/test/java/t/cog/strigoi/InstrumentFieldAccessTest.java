package t.cog.strigoi;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class InstrumentFieldAccessTest {
    private static boolean hasGet = false;
    private static boolean hasPut = false;

    public static void accessField() {}

    public static void accessField(boolean isPut) {
        if (isPut) {
            hasPut = true;
            hasGet = false;
        } else  {
            hasGet = true;
            hasPut = false;
        }
    }

    @Before
    public void init() {
        hasGet = false;
        hasPut = false;
    }

    @Test
    public void empty() throws NoSuchMethodException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException {
        ClassLoader cl = InstrumentFieldAccess.make()
                .onAccess(InstrumentFieldAccessTest.class, "accessField")
                .buildClassLoader();
        Class<?> cls = cl.loadClass("t.cog.strigoi.Foo");
        cls.newInstance();
    }

    @Test
    public void checkGetterSetter() throws NoSuchMethodException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException, InvocationTargetException {
        ClassLoader cl = InstrumentFieldAccess.make()
                        .onAccess(InstrumentFieldAccessTest.class, "accessField", ParameterType.IS_PUT)
                        .buildClassLoader("t.cog.strigoi.Foo"::equals);
        Class<?> cls = cl.loadClass("t.cog.strigoi.Foo");
        Method getX = cls.getMethod("getX", new Class[0]);
        Method setX = cls.getMethod("setX", new Class[]{int.class});
        Object obj = cls.newInstance();
        getX.invoke(obj, new Object[0]);
        assertTrue(hasGet);
        assertFalse(hasPut);
        setX.invoke(obj, new Object[]{1});
        assertFalse(hasGet);
        assertTrue(hasPut);
    }

    @Test
    public void checkGetterSetterLambda() throws NoSuchMethodException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException, InvocationTargetException {
        ClassLoader cl = InstrumentFieldAccess.make()
                .onAccess(InstrumentFieldAccessTest.class, "accessField", ParameterType.IS_PUT)
                .buildClassLoader("t.cog.strigoi.Foo"::equals);
        Class<?> cls = cl.loadClass("t.cog.strigoi.Foo");
        Method getX = cls.getMethod("lambdaRead", new Class[0]);
        Method setX = cls.getMethod("lambdaWrite", new Class[]{int.class});
        Object obj = cls.newInstance();
        getX.invoke(obj, new Object[0]);
        assertTrue(hasGet);
        assertFalse(hasPut);
        setX.invoke(obj, new Object[]{1});
        assertFalse(hasGet);
        assertTrue(hasPut);
    }
}
