package t.cog.strigoi;

import java.util.function.IntSupplier;

public class Foo {
    private int x;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void handleArrayL(int size) {
        long[] array = new long[size];
        array[size / 2] = size;
        long foo = array[size - 3];
        setX((int) foo);
    }

    public void handleArrayI(int size) {
        int[] array = new int[size];
        array[size / 2] = size;
        int foo = array[size - 3];
        setX(foo);
    }

    public void lambdaWrite(int amount) {
        Runnable run = () -> {
            x = amount;
        };
        run.run();
    }

    public int lambdaRead() {
        IntSupplier run = () -> x;
        return run.getAsInt();
    }
}
