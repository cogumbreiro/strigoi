package t.cog.strigoi;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

public class InstrumentFieldStateAccessTest {
    private static boolean hasGet = false;
    private static boolean hasPut = false;

    public static byte initField() {
        return 10;
    }

    public static void getField(byte tmp, boolean isPut) {
        hasPut = isPut;
        hasGet = ! isPut;
    }

    public static void getField(byte tmp) {
    }

    @Before
    public void init() {
        hasGet = false;
        hasPut = false;
    }

    @Test
    public void minmal() throws NoSuchMethodException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException {
        ClassLoader cl = InstrumentFieldStateAccess
                .make(byte.class)
                // Ensure we only rewrite class Foo
                .fieldSelection(x -> x.getClassName().equals("t.cog.strigoi.Foo"))
                .onInit(InstrumentFieldStateAccessTest.class, "initField")
                .onAccess(InstrumentFieldStateAccessTest.class, "getField")
                .buildClassLoader();
        Class<?> cls = cl.loadClass("t.cog.strigoi.Foo");
        cls.newInstance();
    }

    @Test
    public void empty() throws NoSuchMethodException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException {
        ClassLoader cl = InstrumentFieldStateAccess
                .make(byte.class)
                // Ensure we only rewrite class Foo
                .fieldSelection(x -> x.getClassName().equals("t/cog/strigoi/Foo"))
                .onInit(InstrumentFieldStateAccessTest.class, "initField")
                .onAccess(InstrumentFieldStateAccessTest.class, "getField")
                .fieldSelection(x -> false) // do not accept any field
                .buildClassLoader();
        Class<?> cls = cl.loadClass("t.cog.strigoi.Foo");
        cls.newInstance();
    }

    @Test
    public void happyPath() throws NoSuchMethodException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException {
        ClassLoader cl = InstrumentFieldStateAccess
                .make(byte.class)
                // Ensure we only rewrite class Foo
                .fieldSelection(x -> x.getClassName().equals("t/cog/strigoi/Foo"))
                .onInit(InstrumentFieldStateAccessTest.class, "initField")
                .onAccess(InstrumentFieldStateAccessTest.class, "getField")
                .buildClassLoader("t.cog.strigoi.Foo"::equals);
        Class<?> cls = cl.loadClass("t.cog.strigoi.Foo");
        Field y = cls.getDeclaredField(new FieldLocation("t.cog.strigoi.Foo", "x").getDefaultShadowFieldName());
        Field x = cls.getDeclaredField("x");
        y.setAccessible(true);
        x.setAccessible(true);
        assertNotNull(y);
        Object obj = cls.newInstance();
        assertEquals(0, x.getInt(obj));
        assertEquals(10, y.getByte(obj));
    }

    @Test
    public void checkGetterSetter() throws NoSuchMethodException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException, InvocationTargetException {
        ClassLoader cl = InstrumentFieldStateAccess
                        .make(byte.class)
                        // Ensure we only rewrite class Foo
                        .fieldSelection(x -> x.getClassName().equals("t/cog/strigoi/Foo"))
                        .onInit(InstrumentFieldStateAccessTest.class, "initField")
                        .onAccess(InstrumentFieldStateAccessTest.class, "getField", ParameterType.IS_PUT)
                        .buildClassLoader("t.cog.strigoi.Foo"::equals);
        Class<?> cls = cl.loadClass("t.cog.strigoi.Foo");
        Method getX = cls.getMethod("getX", new Class[0]);
        Method setX = cls.getMethod("setX", new Class[]{int.class});
        Object obj = cls.newInstance();
        getX.invoke(obj, new Object[0]);
        assertTrue(hasGet);
        assertFalse(hasPut);
        setX.invoke(obj, new Object[]{1});
        assertFalse(hasGet);
        assertTrue(hasPut);
    }

    public static AtomicInteger createInt() {
        counter = new AtomicInteger();
        return counter;
    }

    private static AtomicInteger counter;

    public static void accessField(AtomicInteger shadow, boolean isPut, int idx) {
        hasPut = isPut;
        hasGet = ! isPut;
        shadow.incrementAndGet();
        counter = shadow;
    }

    @Test
    public void checkGetterSetterLambda() throws NoSuchMethodException, ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InstantiationException, InvocationTargetException {
        DebugDB db = new DebugDB();
        ClassLoader cl = InstrumentFieldStateAccess
                .make(AtomicInteger.class)
                .setDebugDB(db)
                // Ensure we only rewrite class Foo
                .fieldSelection(x -> x.getClassName().equals("t/cog/strigoi/Foo"))
                .onInit(InstrumentFieldStateAccessTest.class, "createInt")
                .onAccess(InstrumentFieldStateAccessTest.class, "accessField", ParameterType.IS_PUT, ParameterType.DEBUG_INFO)
                .buildClassLoader("t.cog.strigoi.Foo"::equals);
        Class<?> cls = cl.loadClass("t.cog.strigoi.Foo");
        Method getX = cls.getMethod("lambdaRead", new Class[0]);
        Method setX = cls.getMethod("lambdaWrite", new Class[]{int.class});
        Object obj = cls.newInstance();
        getX.invoke(obj, new Object[0]);
        assertTrue(hasGet);
        assertFalse(hasPut);
        assertEquals(1, counter.get());
        setX.invoke(obj, new Object[]{1});
        assertFalse(hasGet);
        assertTrue(hasPut);
        assertEquals(2, counter.get());
    }
}
