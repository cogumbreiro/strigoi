package t.cog.strigoi;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

public class DebugDB {
    private final List<Supplier<String>> accessSites = new ArrayList<>();
    private final Set<Supplier<String>> visitedSites = new HashSet<>();
    public synchronized int indexOf(Supplier<String> debugInfo) {
        if (visitedSites.contains(debugInfo)) {
            return accessSites.indexOf(debugInfo);
        }
        int index = accessSites.size();
        accessSites.add(debugInfo);
        visitedSites.add(debugInfo);
        return index;
    }

    public int indexOfField(FieldAccess debugInfo) {
        return indexOf(debugInfo);
    }

    public int indexOfLine(SourceLocation debugInfo) {
        return indexOf(debugInfo);
    }

    public synchronized Supplier<String> get(int index) {
        return accessSites.get(index);
    }
}
