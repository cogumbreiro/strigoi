package t.cog.strigoi;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

import java.util.function.Predicate;

public class ArrayAccessClassVisitor extends BaseClassVisitor {
    private final OnArrayInitMethod onInit;
    private final OnArrayAccessMethod onAccess;
    private final Predicate<String> shouldInstrument;

    public ArrayAccessClassVisitor(ClassVisitor cv, InstrumentationState state, OnArrayInitMethod onInit, OnArrayAccessMethod onAccess, Predicate<String> shouldInstrument) {
        super(cv, state);
        this.onInit = onInit;
        this.onAccess = onAccess;
        this.shouldInstrument = shouldInstrument;
    }

    @Override
    protected BaseMethodVisitor createMethodVisitor(MethodVisitor mv, int access, String name, String desc) {
        return new ArrayMethodVisitor(mv, access, name, desc, state, onInit, onAccess, shouldInstrument);
    }
}
