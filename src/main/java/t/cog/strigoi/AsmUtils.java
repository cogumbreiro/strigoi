/*
 * Copyright (c) 2009-2011 Ken Wenzel and Mathias Doenitz
 * Copyright (c) 2017 Tiago Cogumbreiro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package t.cog.strigoi;

import org.objectweb.asm.Type;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

class AsmUtils {
    /**
     * Use a thread local cache.
     */
    private final static ThreadLocal<Map<String, Class<?>>> classForDesc = new ThreadLocal<Map<String, Class<?>>>() {
        @Override
        protected Map<String, Class<?>> initialValue() {
            return new HashMap<>();
        }
    };

    public static Class<?> getClassForInternalName(String classDesc) {
        Map<String, Class<?>> cache = classForDesc.get();
        Class<?> clazz = cache.get(classDesc);
        if (clazz == null) {
            if (classDesc.charAt(0) == '[') {
                Class<?> compType = getClassForType(Type.getType(classDesc.substring(1)));
                clazz = Array.newInstance(compType, 0).getClass();
            } else {
                String className = classDesc.replace('/', '.');
                try {
                    clazz = AsmUtils.class.getClassLoader().loadClass(className);
                } catch (ClassNotFoundException e) {
                    // If class not found trying the context classLoader
                    try {
                        clazz = Thread.currentThread().getContextClassLoader().loadClass(className);
                    } catch (ClassNotFoundException e2) {
                        throw new RuntimeException("Error loading class '" + className + "' for rule method analysis", e2);
                    }
                }
            }
            cache.put(classDesc, clazz);
        }
        return clazz;
    }

    public static void clearClassCache() {
        classForDesc.get().clear();
    }

    public static Class<?> getClassForType(Type type) {
        switch (type.getSort()) {
            case Type.BOOLEAN:
                return boolean.class;
            case Type.BYTE:
                return byte.class;
            case Type.CHAR:
                return char.class;
            case Type.DOUBLE:
                return double.class;
            case Type.FLOAT:
                return float.class;
            case Type.INT:
                return int.class;
            case Type.LONG:
                return long.class;
            case Type.SHORT:
                return short.class;
            case Type.VOID:
                return void.class;
            case Type.OBJECT:
            case Type.ARRAY:
                return getClassForInternalName(type.getInternalName());
        }
        throw new IllegalStateException(); // should be unreachable
    }
}
