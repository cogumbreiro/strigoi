package t.cog.strigoi;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

/**
 *
 * <p>Adds ghost fields to a class. Whenever a thread accesses a field, the thread
 * invokes a static method, passing a reference of the associated ghost field.</p>
 *
 * <p>
 * For each class range over its fields and optionally create one field per field found.
 * Additionally, whenever a thread accesses a field, it invokes a callback method and supplies
 * the value of the field.
 * </p>
 * @author Tiago Cogumbreiro
 */
class BaseClassVisitor extends ClassVisitor {
    protected final InstrumentationState state;

    /**
     * Create fields given a field creator.
     */
    public BaseClassVisitor(ClassVisitor cv, InstrumentationState state) {
        super(Opcodes.ASM5, cv);
        this.state = state;
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(version, access, name, signature, superName, interfaces);
        state.setClassName(name);
    }

    @Override
    public MethodVisitor visitMethod(final int access, final String name, final String desc, final String signature, final String[] exceptions) {
        // creates a standard method visitor:
        MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
        return createMethodVisitor(mv, access, name, desc);
    }

    protected BaseMethodVisitor createMethodVisitor(MethodVisitor mv, final int access, final String name, final String desc) {
        return new BaseMethodVisitor(mv, access, name, desc, state);
    }

    @Override
    public void visitSource(String fileName, String debug) {
        state.setFileName(fileName);
        super.visitSource(fileName, debug);
    }
}
