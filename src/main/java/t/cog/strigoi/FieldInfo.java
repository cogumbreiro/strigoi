package t.cog.strigoi;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.function.Consumer;

class FieldInfo {
    private final Class<?> fieldType;
    private final FieldLocation field;
    private final boolean isStatic;

    public Class<?> getFieldType() {
        return fieldType;
    }

    public FieldLocation getField() {
        return field;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public FieldInfo(Class<?> fieldType, FieldLocation field, boolean isStatic) {
        this.fieldType = fieldType;
        this.field = field;
        this.isStatic = isStatic;
    }

    public void invokeGetState(MethodVisitor mv) {
        if (isStatic) {
            field.visitFieldInsn(mv, Opcodes.GETSTATIC, fieldType);
        } else {
            field.visitFieldInsn(mv, Opcodes.GETFIELD, fieldType);
        }
    }

    public void set(MethodVisitor mv, Consumer<MethodVisitor> setter) {
        if (isStatic) {
            setter.accept(mv);
            field.visitFieldInsn(mv, Opcodes.PUTSTATIC, fieldType);
        } else {
            Util.pushThis(mv);
            setter.accept(mv);
            field.visitFieldInsn(mv, Opcodes.PUTFIELD, fieldType);
        }
    }
}
