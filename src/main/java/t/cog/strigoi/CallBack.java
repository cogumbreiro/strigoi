package t.cog.strigoi;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

class CallBack {
    private final Class<?> type;
    private String methodName;

    public CallBack(Class<?> type, String methodName) {
        this.type = type;
        this.methodName = methodName;
    }

    public Class<?> getType() {
        return type;
    }

    public String getMethodName() {
        return methodName;
    }

    public Method geStatictMethod(Class<?>[] params) throws NoSuchMethodException {
        Method method = type.getMethod(methodName, params);
        if (! Modifier.isStatic(method.getModifiers())) {
            throw new NoSuchMethodException("Expecting a static method, but got: " + method);
        }
        return method;
    }
}
