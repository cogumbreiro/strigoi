package t.cog.strigoi;

class OnArrayInitMethod extends MethodInfo {
    public OnArrayInitMethod(CallBack cb) throws NoSuchMethodException {
        super(cb, void.class, new Class[]{Object.class});
    }
}
