package t.cog.strigoi;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

/**
 * Bridges between Java-reflection methods and ASM methods.
 * Uses reflection to query a {@link java.lang.reflect.Method} supplied in {@link CallBack} and the given signature.
 * Allows for simple invocation of method calls.
 */
class MethodInfo {
    private final java.lang.reflect.Method reflMethod;
    private final org.objectweb.asm.commons.Method asmMethod;
    private final String className;

    /**
     * Creates a method
     * @param cb
     * @param returnType The return type of the method.
     * @param args The parameters of the method.
     * @throws NoSuchMethodException Could not
     */
    public MethodInfo(CallBack cb, Class returnType, Class[] args) throws NoSuchMethodException {
        this.reflMethod = cb.geStatictMethod(args);
        Type[] types = new Type[args.length];
        for (int i = 0; i < args.length; i++) {
            types[i] = Type.getType(args[i]);
        }
        this.asmMethod = new org.objectweb.asm.commons.Method(cb.getMethodName(), Type.getType(returnType), types);
        this.className = Type.getInternalName(cb.getType());
    }

    /**
     * Utility constructor for a method call with zero parameters.
     * @param cb
     * @param returnType
     * @throws NoSuchMethodException
     */
    public MethodInfo(CallBack cb, Class returnType) throws NoSuchMethodException {
        this(cb, returnType, new Class[0]);
    }

    /**
     * Returns the Java reflection method.
     * @return
     */
    public java.lang.reflect.Method getReflMethod() {
        return reflMethod;
    }

    /**
     * Returns the ASM method.
     * @return
     */
    public org.objectweb.asm.commons.Method getAsmMethod() {
        return asmMethod;
    }

    /**
     * Generates a static call to the given method.
     * @param mv
     */
    public void invokeStatic(MethodVisitor mv) {
        mv.visitMethodInsn(Opcodes.INVOKESTATIC, className,
                asmMethod.getName(), asmMethod.getDescriptor(), false);
    }
}
