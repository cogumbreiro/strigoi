package t.cog.strigoi;

import org.objectweb.asm.*;

import java.util.function.Predicate;

/**
 *
 * <p>Adds ghost fields to a class. Whenever a thread accesses a field, the thread
 * invokes a static method, passing a reference of the associated ghost field.</p>
 *
 * <p>
 * For each class range over its fields and optionally create one field per field found.
 * Additionally, whenever a thread accesses a field, it invokes a callback method and supplies
 * the value of the field.
 * </p>
 * @author Tiago Cogumbreiro
 */
class FieldAccessClassVisitor extends BaseClassVisitor {
    private final OnFieldAccessMethod accessMethod;
    private final Predicate<FieldLocation> creator;

    /**
     * Create fields given a field creator.
     */
    public FieldAccessClassVisitor(ClassVisitor cv, InstrumentationState state, OnFieldAccessMethod accessMethod, Predicate<FieldLocation> creator) {
        super(cv, state);
        this.accessMethod = accessMethod;
        this.creator = creator;
    }

    @Override
    protected BaseMethodVisitor createMethodVisitor(MethodVisitor mv, int access, String name, String desc) {
        return new FieldAccessMethodVisitor(mv, access, name, desc, state, (opCode, fieldLoc) -> {
            if (creator.test(fieldLoc)) {
                Parameters params = new Parameters(state);
                params.pushArrayParams(mv, opCode);
                accessMethod.invokeStatic(mv);
            }
        });
    }
}
