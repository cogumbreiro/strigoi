package t.cog.strigoi;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import static org.objectweb.asm.Opcodes.*;

/**
 * Utility class that simplifies loading primitve values.
 * @author Tiago Cogumbreiro
 */
class Util {
    public static void push(MethodVisitor mv, final byte value) {
        if  (0 <= value && value <= 5) {
            final int code;
            switch(value) {
                case 0: code = ICONST_0; break;
                case 1: code = ICONST_1; break;
                case 2: code = ICONST_2; break;
                case 3: code = ICONST_3; break;
                case 4: code = ICONST_4; break;
                case 5: code = ICONST_5; break;
                default:
                    throw new IllegalStateException();

            }
            mv.visitInsn(code);
        } else {
            mv.visitIntInsn(BIPUSH, value);
        }
    }
    public static void push(MethodVisitor mv, final short value) {
        if (Byte.MIN_VALUE <= value && value <= Byte.MAX_VALUE) {
            push(mv, (byte) value);
        } else {
            mv.visitIntInsn(Opcodes.SIPUSH, value);
        }
    }
    public static void push(MethodVisitor mv, final int value) {
        if (Short.MIN_VALUE <= value && value <= Short.MAX_VALUE) {
            push(mv, (short) value);
        } else {
            mv.visitLdcInsn(Integer.valueOf(value));
        }
    }

    public static void push(MethodVisitor mv, final long value) {
        if (Integer.MIN_VALUE <= value && value <= Integer.MAX_VALUE) {
            push(mv, (int) value);
        } else {
            mv.visitLdcInsn(Long.valueOf(value));
        }
    }

    public static void push(MethodVisitor mv, final boolean value) {
        mv.visitInsn(value ? Opcodes.ICONST_1 : Opcodes.ICONST_0);
    }

    public static void push(MethodVisitor mv, final String value) {
        if (value == null) {
            mv.visitInsn(Opcodes.ACONST_NULL);
        } else {
            mv.visitLdcInsn(value);
        }
    }

    public static void pushThis(MethodVisitor mv) {
        mv.visitVarInsn(Opcodes.ALOAD, 0);
    }

    public static void pushNull(MethodVisitor mv) {
        mv.visitInsn(Opcodes.ACONST_NULL);
    }

    public static boolean isPublic(int access) {
        return (access & Opcodes.ACC_PUBLIC) != 0;
    }
    public static boolean isStatic(int access) {
        return (access & Opcodes.ACC_STATIC) != 0;
    }
    public static Class<?> fromDescriptor(String descriptor) {
        Type type = Type.getType(descriptor);
        return AsmUtils.getClassForType(type);
    }

    private static Class classof(int code) {
        switch(code) {
        case AALOAD:
        case AASTORE:
            return Object.class;
        case BALOAD:
        case BASTORE:
            return boolean.class;
        case CALOAD:
        case CASTORE:
            return char.class;
        case FALOAD:
        case FASTORE:
            return float.class;
        case IALOAD:
        case IASTORE:
            return int.class;
        case SALOAD:
        case SASTORE:
            return short.class;
        case DALOAD:
        case DASTORE:
            return double.class;
        case LALOAD:
        case LASTORE:
            return long.class;
        default:
            throw new IllegalArgumentException(String.format("Unknown opcode: %x", code));
        }
    }

    public static Type typeof(int opCode) {
        return Type.getType(classof(opCode));
    }
}
