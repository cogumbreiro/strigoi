package t.cog.strigoi;

class OnFieldAccessMethod extends MethodInfo {
    public OnFieldAccessMethod(CallBack cb, ParameterType[] parameters) throws NoSuchMethodException {
        super(cb, void.class,ParameterType.asArray(parameters));
    }
}
