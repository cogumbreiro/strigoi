package t.cog.strigoi;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

public class Instrumentation {
    private static class Transformer implements ClassFileTransformer {
        private final UnaryOperator<ClassVisitor> visitor;
        private final Predicate<String> instrumentClass;

        public Transformer(UnaryOperator<ClassVisitor> visitor, Predicate<String> instrumentClass) {
            this.visitor = visitor;
            this.instrumentClass = instrumentClass;
        }

        @Override
        public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
                                ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
            if (instrumentClass.test(className)) {
                ClassReader cr = new ClassReader(classfileBuffer);
                ClassWriter cw = new ClassWriter(cr, ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
                cr.accept(visitor.apply(cw), ClassReader.SKIP_FRAMES);
                return cw.toByteArray();
            } else {
                // Returning null means nothing to do
                return null;
            }
        }

    }

    public static ClassFileTransformer asClassFileTransformer(UnaryOperator<ClassVisitor> visitor, Predicate<String> instrumentClass) {
        return new Transformer(visitor, instrumentClass);
    }
}
