package t.cog.strigoi;

import org.objectweb.asm.ClassVisitor;

import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 *
 * <p>Adds ghost fields to a class. Whenever a thread accesses a field, the thread
 * invokes a static method, passing a reference of the associated ghost field.</p>
 *
 * <p>
 * For each class range over its fields and optionally create one field per field found.
 * Additionally, whenever a thread accesses a field, it invokes a callback method and supplies
 * the value of the field.
 * </p>
 * @author Tiago Cogumbreiro
 */
public class InstrumentFieldAccess {
    private final DebugDB db;
    private final CallBack access;
    private final ParameterType[] parameters;
    private final Predicate<FieldLocation> instrument;

    private InstrumentFieldAccess() {
        this(null, null, null, x -> true);
    }

    public static InstrumentFieldAccess make() {
        return new InstrumentFieldAccess();
    }

    public InstrumentFieldAccess(DebugDB db, CallBack access, ParameterType[] parameters, Predicate<FieldLocation> creator) {
        this.db = db;
        this.access = access;
        this.parameters = parameters;
        this.instrument = creator;
    }

    public InstrumentFieldAccess onAccess(Class<?> cls, String methodName, ParameterType... parameters) {
        return new InstrumentFieldAccess(db, new CallBack(cls, methodName), parameters, instrument);
    }

    /**
     * By default all field accesses are instrumented. This predicate ensures that
     * we can select which ones to fieldSelection.
     * @param creator
     * @return
     */
    public InstrumentFieldAccess fieldSelection(Predicate<FieldLocation> creator) {
        return new InstrumentFieldAccess(db, access, parameters, creator);
    }

    public InstrumentFieldAccess setDebugDB(DebugDB newDb) {
        return new InstrumentFieldAccess(newDb, access, parameters, instrument);
    }

    public UnaryOperator<ClassVisitor> buildClassVisitor() throws NoSuchMethodException {
        if (access == null) {
            throw new IllegalStateException("Invoke onAccess() first.");
        }
        if (db == null) {
            int i = 0;
            for (ParameterType param : parameters) {
                if (param == ParameterType.DEBUG_INFO) {
                    throw new IllegalArgumentException("Supplied a null debug-info, but the " + i + "-th parameter is requesting for debug-info.");
                }
                i++;
            }
        }
        OnFieldAccessMethod accessMethod = new OnFieldAccessMethod(access, parameters);
        InstrumentationState state = new InstrumentationState(parameters);
        state.setDb(db);
        return visitor -> new FieldAccessClassVisitor(visitor, state, accessMethod, instrument);
    }

    public ClassLoader buildClassLoader() throws NoSuchMethodException {
        return new InstrumentationClassLoader(buildClassVisitor());
    }

    public ClassLoader buildClassLoader(Predicate<String> instrument) throws NoSuchMethodException {
        return new InstrumentationClassLoader(buildClassVisitor(), instrument);
    }
}
