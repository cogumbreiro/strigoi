package t.cog.strigoi;

class OnFieldStateInitMethod<T> extends MethodInfo {
    private final Class<T> ghostType;
    public OnFieldStateInitMethod(CallBack cb, Class<T> ghostType) throws NoSuchMethodException {
        super(cb, ghostType);
        this.ghostType = ghostType;
    }

    public Class<T> getGhostType() {
        return ghostType;
    }
}
