package t.cog.strigoi;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.util.TraceClassVisitor;

import java.io.PrintWriter;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 *
 * <p>Adds ghost fields to a class. Whenever a thread accesses a field, the thread
 * invokes a static method, passing a reference of the associated ghost field.</p>
 *
 * <p>
 * For each class range over its fields and optionally create one field per field found.
 * Additionally, whenever a thread accesses a field, it invokes a callback method and supplies
 * the value of the field.
 * </p>
 * @author Tiago Cogumbreiro
 */
public class InstrumentFieldStateAccess<T> {
    private final Class<T> ghostType;
    private final DebugDB db;
    private final CallBack init;
    private final CallBack access;
    private final ParameterType[] parameters;
    private final Function<FieldLocation, Optional<String>> fieldSelector;
    private final boolean debug;
    // By default we instrument all fields
    private static final Function<FieldLocation, Optional<String>> DEFAULT_SELECTOR = defaultFieldSelection(x->true);

    private InstrumentFieldStateAccess(Class<T> cls) {
        this(null, cls, null, null, null, DEFAULT_SELECTOR, false);
    }

    private InstrumentFieldStateAccess(DebugDB db, Class<T> ghostType, CallBack init, CallBack access, ParameterType[] parameters, Function<FieldLocation, Optional<String>> fieldSelector, boolean debug) {
        if (ghostType == null) {
            throw new IllegalArgumentException("Cannot accept a null ghost type.");
        }
        this.db = db;
        this.ghostType = ghostType;
        this.init = init;
        this.access = access;
        this.parameters = parameters;
        this.fieldSelector = fieldSelector;
        this.debug = debug;
    }

    public InstrumentFieldStateAccess<T> onInit(Class<?> cls, String methodName) {
        return new InstrumentFieldStateAccess<>(db, ghostType, new CallBack(cls, methodName), access, parameters, fieldSelector, debug);
    }

    public InstrumentFieldStateAccess<T> onAccess(Class<?> cls, String methodName, ParameterType... parameters) {
        return new InstrumentFieldStateAccess<>(db, ghostType, init, new CallBack(cls, methodName), parameters, fieldSelector, debug);
    }

    public InstrumentFieldStateAccess<T> setDebugDB(DebugDB db) {
        return new InstrumentFieldStateAccess<>(db, ghostType, init, access, parameters, fieldSelector, debug);
    }

    private static Function<FieldLocation, Optional<String>> defaultFieldSelection(final Predicate<FieldLocation> instrument) {
        return x -> instrument.test(x) ? Optional.of(x.getDefaultShadowFieldName()) : Optional.empty();
    }

    public InstrumentFieldStateAccess<T> fieldSelection(final Predicate<FieldLocation> instrument) {
        return new InstrumentFieldStateAccess<>(db, ghostType, init, access, parameters, defaultFieldSelection(instrument), debug);
    }

    public InstrumentFieldStateAccess<T> enableDebug() {
        return new InstrumentFieldStateAccess<>(db, ghostType, init, access, parameters, fieldSelector, true);
    }

    private InstrumentFieldStateAccess<T> fieldSelectionEx(Function<FieldLocation, Optional<String>> creator) {
        return new InstrumentFieldStateAccess<>(db, ghostType, init, access, parameters, creator, debug);
    }

    public UnaryOperator<ClassVisitor> buildClassVisitor() throws NoSuchMethodException {
        if (init == null) {
            throw new IllegalStateException("Invoke onInit() first.");
        }
        if (access == null) {
            throw new IllegalStateException("Invoke onAccess() first.");
        }
        if (fieldSelector == null) {
            throw new IllegalStateException("Invoke fieldSelection() first.");
        }
        OnFieldStateInitMethod<T> onInit = new OnFieldStateInitMethod<>(init, ghostType);
        OnFieldStateAccessMethod<T> onAccess = new OnFieldStateAccessMethod<>(access, ghostType, parameters);
        return visitor -> {
            InstrumentationState state = new InstrumentationState(parameters);
            state.setDb(db);
            if (debug) {
                visitor = new TraceClassVisitor(visitor, new PrintWriter(System.out));
            }
            return new FieldStateClassVisitor<>(visitor, state, onInit, onAccess, fieldSelector);
        };
    }

    public ClassLoader buildClassLoader() throws NoSuchMethodException {
        return new InstrumentationClassLoader(buildClassVisitor());
    }
    public ClassLoader buildClassLoader(Predicate<String> instrument) throws NoSuchMethodException {
        return new InstrumentationClassLoader(buildClassVisitor(), instrument);
    }

    public static <T> InstrumentFieldStateAccess<T> make(Class<T> cls) {
        return new InstrumentFieldStateAccess<T>(cls);
    }
}
