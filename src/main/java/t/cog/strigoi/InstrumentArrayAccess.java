package t.cog.strigoi;

import org.objectweb.asm.ClassVisitor;

import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 *
 * <p>Adds ghost fields to a class. Whenever a thread accesses a field, the thread
 * invokes a static method, passing a reference of the associated ghost field.</p>
 *
 * <p>
 * For each class range over its fields and optionally create one field per field found.
 * Additionally, whenever a thread accesses a field, it invokes a callback method and supplies
 * the value of the field.
 * </p>
 * @author Tiago Cogumbreiro
 */
public class InstrumentArrayAccess {
    private final DebugDB db;
    private final CallBack init;
    private final CallBack access;
    private final ParameterType[] parameters;
    private final Predicate<String> shouldInstrument;

    private InstrumentArrayAccess() {
        this(null, null, null, null, x -> true);
    }

    public static InstrumentArrayAccess make() {
        return new InstrumentArrayAccess();
    }

    public InstrumentArrayAccess(DebugDB db, CallBack init, CallBack access, ParameterType[] parameters, Predicate<String> creator) {
        this.db = db;
        this.init = init;
        this.access = access;
        this.parameters = parameters;
        this.shouldInstrument = creator;
    }

    public InstrumentArrayAccess onAccess(Class<?> cls, String methodName, ParameterType... parameters) {
        return new InstrumentArrayAccess(db, init, new CallBack(cls, methodName), parameters, shouldInstrument);
    }

    public InstrumentArrayAccess onInit(Class<?> cls, String methodName) {
        return new InstrumentArrayAccess(db, new CallBack(cls, methodName), access, parameters, shouldInstrument);
    }

    public InstrumentArrayAccess setDebugDB(DebugDB newDB) {
        return new InstrumentArrayAccess(newDB, init, access, parameters, shouldInstrument);
    }


    /**
     * By default all field accesses are instrumented. This predicate ensures that
     * we can select which ones to fieldSelection.
     * @param shouldInstrument
     * @return
     */
    public InstrumentArrayAccess fieldSelection(Predicate<String> shouldInstrument) {
        return new InstrumentArrayAccess(db, init, access, parameters, shouldInstrument);
    }

    public UnaryOperator<ClassVisitor> buildClassVisitor() throws NoSuchMethodException {
        if (access == null) {
            throw new IllegalStateException("Invoke onAccess() first.");
        }
        if (db == null) {
            int i = 0;
            for (ParameterType param : parameters) {
                if (param == ParameterType.DEBUG_INFO) {
                    throw new IllegalArgumentException("Supplied a null debug-info, but the " + i + "-th parameter is requesting for debug-info.");
                }
                i++;
            }
        }
        OnArrayInitMethod onInit = new OnArrayInitMethod(init);
        OnArrayAccessMethod onAccess = new OnArrayAccessMethod(access, parameters);
        InstrumentationState state = new InstrumentationState(parameters);
        state.setDb(db);
        return visitor -> new ArrayAccessClassVisitor(visitor, state, onInit, onAccess, shouldInstrument);
    }

    public ClassLoader buildClassLoader() throws NoSuchMethodException {
        return new InstrumentationClassLoader(buildClassVisitor());
    }

    public ClassLoader buildClassLoader(Predicate<String> instrument) throws NoSuchMethodException {
        return new InstrumentationClassLoader(buildClassVisitor(), instrument);
    }
}
