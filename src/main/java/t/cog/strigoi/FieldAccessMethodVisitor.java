package t.cog.strigoi;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

class FieldAccessMethodVisitor extends BaseMethodVisitor {
    public interface Handler {
        void onAccessField(int opCode, FieldLocation field);
    }

    private final Handler handler;

    public FieldAccessMethodVisitor(MethodVisitor mv, int access, String methodName, String desc, InstrumentationState state, Handler handler) {
        super(mv, access, methodName, desc, state);
        this.handler = handler;
    }

    @Override
    public void visitFieldInsn(final int opCode, final String owner, final String name, final String desc) {
        switch (opCode) {
            case GETFIELD:
            case GETSTATIC:
            case PUTFIELD:
            case PUTSTATIC:
                FieldLocation fieldLoc = new FieldLocation(owner, name);
                handler.onAccessField(opCode, fieldLoc);
        }
        super.visitFieldInsn(opCode, owner, name, desc);
    }

    @Override
    public void visitLineNumber(int line, Label start) {
        state.setLineNum(line);
    }
}
