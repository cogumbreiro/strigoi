package t.cog.strigoi;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

/**
 * The location of a field.
 */
public class FieldLocation {
    private final String className;
    private final String fieldName;
    private static final String PREFIX = "$strigoi_";

    public FieldLocation(Class cls, String fieldName) {
        this(cls.getName(), fieldName);
    }

    public FieldLocation(String className, String fieldName) {
        this.className = className.replace('.', '/');
        this.fieldName = fieldName;
    }

    public String getClassName() {
        return className;
    }

    public String getFieldName() {
        return fieldName;
    }

    public FieldLocation setFieldName(String fieldName) {
        return new FieldLocation(className, fieldName);
    }

    public String getDefaultShadowFieldName() {
        String[] parts = getClassName().split("/");
        int lastElem = parts.length;
        StringBuilder builder = new StringBuilder();
        for (String part : parts) {
            if (lastElem == 1) {
                builder.append('_');
                builder.append(part);
                break;
            } else {
                builder.append(part.charAt(0));
            }
            lastElem--;
        }
        if (parts.length > 0) {
            builder.append('_');
        }
        return PREFIX + builder + getFieldName();
    }

    /**
     * Gets/puts a ghost field.
     * @param mv
     * @param opcode
     */
    void visitFieldInsn(MethodVisitor mv, int opcode, Class<?> ghostType) {
        mv.visitFieldInsn(opcode, className, fieldName, Type.getDescriptor(ghostType));
    }

    /**
     * Creates a ghost field
     * @param cv
     * @param access
     */
    void visitField(ClassVisitor cv, int access, Class<?> ghostType) {
        // Creates the shadow field
        cv.visitField(access, fieldName, Type.getDescriptor(ghostType), null, null);
    }

    @Override
    public String toString() {
        return className + "#" + fieldName;
    }
}
