package t.cog.strigoi;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.AdviceAdapter;

class BaseMethodVisitor extends AdviceAdapter {
    protected final InstrumentationState state;

    public BaseMethodVisitor(MethodVisitor mv, int access, String methodName, String desc, InstrumentationState state) {
        super(Opcodes.ASM5, mv, access, methodName, desc);
        this.state = state;
    }

    @Override
    public void visitLineNumber(int line, Label start) {
        state.setLineNum(line);
    }
}
