package t.cog.strigoi;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

class InstrumentationClassLoader extends ClassLoader {

    private byte[] instrumentClass(String className) {
        String resourceName = className.replace('.', '/') + ".class";
        try {
            InputStream is = getResourceAsStream(resourceName);
            ClassReader cr = new ClassReader(is);
            ClassWriter cw = new ClassWriter(cr, ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
            cr.accept(visitor.apply(cw), ClassReader.SKIP_FRAMES);
            return cw.toByteArray();
        } catch (IOException e) {
            throw new IllegalArgumentException("Error instrumenting class " + className + " resource name: " + resourceName, e);
        }
    }

    private final Predicate<String> instrument;
    private final UnaryOperator<ClassVisitor> visitor;

    public InstrumentationClassLoader(UnaryOperator<ClassVisitor> visitor) {
        this(visitor, x -> ! x.startsWith("java.lang."));
    }

    public InstrumentationClassLoader(UnaryOperator<ClassVisitor> visitor, Predicate<String> instrument) {
        super();
        this.visitor = visitor;
        this.instrument = instrument;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        if (instrument.test(name)) {
            byte[] byteBuffer = instrumentClass(name);
            return defineClass(name, byteBuffer, 0, byteBuffer.length);
        }
        return super.loadClass(name);
    }
}
