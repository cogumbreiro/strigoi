package t.cog.strigoi;

import java.util.function.Supplier;

/**
 * Information about the a certain field information.
 */
public class FieldAccess implements Supplier<String> {
    private final SourceLocation source;
    private final FieldLocation field;

    public FieldAccess(FieldLocation field, SourceLocation source) {
        this.field = field;
        this.source = source;
    }

    public FieldLocation getFieldLocation() {
        return field;
    }

    public SourceLocation getSourceLocation() {
        return source;
    }

    @Override
    public String get() {
        return source + ":" + field;
    }

    @Override
    public String toString() {
        return get();
    }
}
