package t.cog.strigoi;

import java.util.ArrayList;
import java.util.Arrays;

class OnArrayAccessMethod extends MethodInfo {
    private static Class[] asClass(ParameterType[] params) {
        ArrayList<Class> elems = new ArrayList<>(Arrays.asList(Object.class, int.class));
        elems.addAll(Arrays.asList(ParameterType.asArray(params)));
        return elems.toArray(new Class[0]);
    }
    public OnArrayAccessMethod(CallBack cb, ParameterType[] params) throws NoSuchMethodException {
        super(cb, void.class, asClass(params));
    }
}
