package t.cog.strigoi;

public enum ParameterType {
    IS_PUT {
        @Override
        public Class<?> getType() {
            return boolean.class;
        }
    },
    DEBUG_INFO {
        @Override
        public Class<?> getType() {
            return int.class;
        }
    };

    /**
     * Returns the type of the parameter.
     * @return
     */
    public abstract Class<?> getType();

    public static Class[] asArray(ParameterType[] parameters) {
        Class[] expected = new Class[parameters.length];
        // Load Java accessMethod
        for (int i = 0; i < parameters.length; i++) {
            expected[i] = parameters[i].getType();
        }
        return expected;
    }
}
