package t.cog.strigoi;

/**
 * Internal instrumentation state used by the various visitors.
 */
class InstrumentationState {
    private String className;
    private String fileName;
    private DebugDB db;
    private int lineNum;
    private final ParameterType[] parameterTypes;

    public InstrumentationState(ParameterType[] parameters) {
        this.parameterTypes = parameters;
    }

    public String getClassName() {
        return className;
    }

    public FieldLocation makeField(String fieldName) {
        return new FieldLocation(className, fieldName);
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public DebugDB getDb() {
        return db;
    }

    public void setDb(DebugDB db) {
        this.db = db;
    }

    public int getLineNum() {
        return lineNum;
    }

    public void setLineNum(int lineNum) {
        this.lineNum = lineNum;
    }

    public ParameterType[] getParameterTypes() {
        return parameterTypes;
    }
}
