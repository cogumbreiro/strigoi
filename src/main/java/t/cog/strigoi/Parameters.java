package t.cog.strigoi;

import org.objectweb.asm.MethodVisitor;
import static org.objectweb.asm.Opcodes.*;
class Parameters {
    private final InstrumentationState state;

    public Parameters(InstrumentationState state) {
        this.state = state;
    }

    private static boolean isPut(int code) {
        switch (code) {
            case DASTORE:
            case LASTORE:
            case AASTORE:
            case BASTORE:
            case CASTORE:
            case FASTORE:
            case IASTORE:
            case SASTORE:
            case PUTFIELD:
            case PUTSTATIC:
                return true;
            case AALOAD:
            case BALOAD:
            case CALOAD:
            case FALOAD:
            case IALOAD:
            case SALOAD:
            case DALOAD:
            case LALOAD:
            case GETFIELD:
            case GETSTATIC:
                return false;
            default:
                throw new IllegalStateException(String.format("Unknown upcode: %x", code));
        }
    }

    public void pushFieldStateParams(MethodVisitor mv, FieldLocation field, int opCode) {
        pushAllParams(mv, isPut(opCode), field);
    }

    public void pushArrayParams(MethodVisitor mv, int opCode) {
        pushFieldStateParams(mv, null, opCode);
    }

    private int getDebugIdx(FieldLocation field) {
        if (state.getDb() == null) {
            throw new IllegalStateException("Requesting Debug ID, but no DebugDB object defined.");
        }
        SourceLocation location = new SourceLocation(state.getFileName(), state.getLineNum());
        if (field == null) {
            return state.getDb().indexOf(location);
        }
        return state.getDb().indexOf(new FieldAccess(field, location));
    }

    private void pushAllParams(MethodVisitor mv, boolean isPut, FieldLocation field) {
        for (ParameterType paramType : state.getParameterTypes()) {
            switch(paramType) {
                case IS_PUT:
                    Util.push(mv, isPut);
                    break;
                case DEBUG_INFO:
                    int debugIdx = getDebugIdx(field);
                    Util.push(mv, debugIdx);
                    break;
                default:
                    throw new IllegalStateException("Unsupported parameter type: " + paramType);
            }
        }
    }
}
