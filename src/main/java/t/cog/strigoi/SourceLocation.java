package t.cog.strigoi;

import java.util.function.Supplier;

/**
 * Location inside a source file.
 */
public class SourceLocation implements Supplier<String> {
    private final String fileName;
    private final int lineNumber;

    public SourceLocation(String fileName, int lineNumber) {
        this.fileName = fileName;
        this.lineNumber = lineNumber;
    }

    public String getFileName() {
        return fileName;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    @Override
    public String get() {
        return fileName + ":" + lineNumber;
    }

    @Override
    public String toString() {
        return get();
    }
}
