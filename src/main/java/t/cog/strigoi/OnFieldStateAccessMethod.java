package t.cog.strigoi;

import java.util.ArrayList;
import java.util.Arrays;

class OnFieldStateAccessMethod<T> extends MethodInfo {
    public OnFieldStateAccessMethod(CallBack cb, Class<T> ghostType, ParameterType[] parameters) throws NoSuchMethodException {
        super(cb, void.class, params(ghostType, parameters));
    }
    private static Class[] params(Class arg, ParameterType[] params) {
        ArrayList<Class> result = new ArrayList<>();
        result.add(arg);
        result.addAll(Arrays.asList(ParameterType.asArray(params)));
        return result.toArray(new Class[0]);
    }
}
