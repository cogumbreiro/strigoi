package t.cog.strigoi;

import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

import java.util.function.Predicate;

/**
 * Adds instrumentation around memory accesses.
 *
 * @author Tiago Cogumbreiro, Rishi Surendran
 */
class ArrayMethodVisitor extends BaseMethodVisitor {
    private final InstrumentationState state;
    private final OnArrayAccessMethod onAccess;
    private final OnArrayInitMethod onInit;
    private final Predicate<String> shouldInstrument;

    public ArrayMethodVisitor(final MethodVisitor mv, int access, String methodName, String desc, InstrumentationState state, OnArrayInitMethod onInit, OnArrayAccessMethod onAccess, Predicate<String> shouldInstrument) {
        super(mv, access, methodName, desc, state);
        this.state = state;
        this.onInit = onInit;
        this.onAccess = onAccess;
        this.shouldInstrument = shouldInstrument;
    }

    /**
     * Instrument array element accesses
     *
     * @param opcode
     */
    @Override
    public void visitInsn(int opcode) {
        Parameters params = new Parameters(state);
        if (shouldInstrument.test(state.getClassName())) {
            switch (opcode) {
                case AALOAD:
                case BALOAD:
                case CALOAD:
                case FALOAD:
                case IALOAD:
                case SALOAD:
                case DALOAD:
                case LALOAD: {
                    // current stack : target index (top)
                    mv.visitInsn(DUP2);
                    params.pushArrayParams(mv, opcode);
                    onAccess.invokeStatic(mv);
                    // current stack: target index (top)
                    mv.visitInsn(opcode);
                    break;
                }

                case DASTORE:
                case LASTORE:
                case AASTORE:
                case BASTORE:
                case CASTORE:
                case FASTORE:
                case IASTORE:
                case SASTORE: {
                    boolean doubleSize = (opcode == DASTORE || opcode == LASTORE);
                    if (doubleSize) {
                        // stack : ..., arrayref, index, double value
                        mv.visitInsn(DUP2_X2);
                        // stack: ..., double value, arrayref, index, double value
                        mv.visitInsn(POP2);
                        // stack: ..., double value, arrayref, index
                        mv.visitInsn(DUP2_X2);
                        // stack: ..., arrayref, index, double value, arrayref, index
                    } else {
                        // stack : ..., arrayref, index, value
                        mv.visitInsn(DUP_X2);
                        // stack : ..., value, arrayref, index, value
                        mv.visitInsn(POP);
                        // stack : ..., value, arrayref, index
                        mv.visitInsn(DUP2_X1);
                        // stack : ..., arrayref, index, value, arrayref, index
                    }
                    // stack : ..., arrayref, index
                    params.pushArrayParams(mv, opcode);
                    onAccess.invokeStatic(mv);
                    mv.visitInsn(opcode);
                    break;
                }
                default:
                    mv.visitInsn(opcode);
            }
        } else
            mv.visitInsn(opcode);
    }

    @Override
    public void visitIntInsn(int opcode, int operand) {
        switch (opcode) {
            case NEWARRAY:
                mv.visitIntInsn(opcode, operand);
                mv.visitInsn(DUP);
                onInit.invokeStatic(mv);
                break;
            default:
                mv.visitIntInsn(opcode, operand);
        }
    }

    @Override
    public void visitTypeInsn(int opcode, String desc) {
        switch (opcode) {
            case ANEWARRAY: {
                mv.visitTypeInsn(opcode, desc);
                mv.visitInsn(DUP);
                onInit.invokeStatic(mv);
                break;
            }
            default:
                mv.visitTypeInsn(opcode, desc);
        }
    }

    @Override
    public void visitMultiANewArrayInsn(String desc, int dims) {
        mv.visitMultiANewArrayInsn(desc, dims);
        mv.visitInsn(DUP);
        onInit.invokeStatic(mv);
    }

    @Override
    public void visitLineNumber(int line, Label start) {
        state.setLineNum(line);
    }
}