package t.cog.strigoi;

import org.objectweb.asm.*;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.commons.AdviceAdapter;

import java.util.*;
import java.util.function.Function;

/**
 *
 * <p>Adds ghost fields to a class. Whenever a thread accesses a field, the thread
 * invokes a static method, passing a reference of the associated ghost field.</p>
 *
 * <p>
 * For each class range over its fields and optionally create one field per field found.
 * Additionally, whenever a thread accesses a field, it invokes a callback method and supplies
 * the value of the field.
 * </p>
 * @author Tiago Cogumbreiro
 */
class FieldStateClassVisitor<T> extends BaseClassVisitor {
    /**
     * Create fields given a field creator.
     * @param cv
     * @param onAccess
     * @param onInit
     */
    public FieldStateClassVisitor(ClassVisitor cv, InstrumentationState state, OnFieldStateInitMethod<T> onInit, OnFieldStateAccessMethod<T> onAccess, Function<FieldLocation, Optional<String>> creator) {
        super(cv, state);
        this.stateFactory = onInit;
        this.onAccess = onAccess;
        this.creator = creator;
    }

    private final List<FieldInfo> dynamicFields = new ArrayList<>();
    private final List<FieldInfo> staticFields = new ArrayList<>();

    private final OnFieldStateInitMethod<T> stateFactory;
    private final OnFieldStateAccessMethod<T> onAccess;
    private final Function<FieldLocation, Optional<String>> creator;

    private Optional<FieldLocation> createFrom(FieldLocation field) {
        Optional<String> result = creator.apply(field);
        if (result.isPresent()) {
            return Optional.of(field.setFieldName(result.get()));
        }
        return Optional.empty();
    }

    /**
     * For each field declaration we can optionally create a new field.
     */
    @Override
    public FieldVisitor visitField(final int access, String name, String desc, String signature, Object value) {
        // define the field as usual, but change its visibility to public
        final FieldVisitor fv = cv.visitField(access, name, desc, signature, value);
        FieldLocation loc = new FieldLocation(state.getClassName(), name);
        Optional<FieldLocation> result = createFrom(loc);
        if (result.isPresent()) {
            FieldLocation target = result.get();
            // Collect the fields to be initialized in <clinit> and <init>
            if (Util.isStatic(access)) {
                staticFields.add(new FieldInfo(stateFactory.getGhostType(), target, true));
            } else {
                dynamicFields.add(new FieldInfo(stateFactory.getGhostType(), target, false));
            }
            // Creates the ghost field
            target.visitField(cv, access, stateFactory.getGhostType());
        }

        return fv;
    }

    /**
     * Initialize the new fields; internally we append some code in the static/dynamic constructor of each class.
     * @param access
     * @param name
     * @param desc
     * @param signature
     * @param exceptions
     * @return
     */
    @Override
    public MethodVisitor visitMethod(final int access, final String name, final String desc, final String signature, final String[] exceptions) {
        // creates a standard method visitor:
        MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
        if ("<init>".equals(name)) {
            // initializes the ghost fields in the dynamic constructor
            return new AdviceAdapter(Opcodes.ASM5, mv, access, name, desc) {
                @Override
                protected void onMethodEnter() {
                    for (FieldInfo field : dynamicFields) {
                        field.set(mv, stateFactory::invokeStatic);
                    }
                }

            };
        } else if ("<clinit>".equals(name)) {
            // initializes the ghost fields in the static constructor
            return new AdviceAdapter(Opcodes.ASM5, mv, access, name, desc) {
                @Override
                protected void onMethodEnter() {
                    for (FieldInfo field : staticFields) {
                        field.set(mv, stateFactory::invokeStatic);
                    }
                }

            };
        }
        return new FieldAccessMethodVisitor(mv, access, name, desc, state, (opCode, fieldLoc) -> {
            Optional<FieldLocation> result = createFrom(fieldLoc);
            Parameters params = new Parameters(state);
            if (result.isPresent()) {
                FieldLocation target = result.get();
                pushState(mv, opCode, desc);
                FieldInfo fieldInfo = new FieldInfo(stateFactory.getGhostType(), target, Util.isStatic(opCode));
                fieldInfo.invokeGetState(mv);
                params.pushFieldStateParams(mv, target, opCode);
                onAccess.invokeStatic(mv);
            }
        });
    }

    private void pushState(MethodVisitor mv, int opCode, String desc) {
        switch(opCode) {
            case Opcodes.GETFIELD:
                // stack : ..., obj
                mv.visitInsn(Opcodes.DUP); // Duplicate this
                // stack : ..., obj, obj
                break;
            case Opcodes.PUTFIELD:
                // Push this
                if (desc.equals(Type.getDescriptor(double.class)) || desc.equals(Type.getDescriptor(long.class))) {
                    // stack : ..., obj, double value
                    mv.visitInsn(Opcodes.DUP_X1);
                    // stack : ..., double value, obj, double value
                    mv.visitInsn(Opcodes.POP2);
                    // stack : ..., double value, obj
                    mv.visitInsn(Opcodes.DUP_X2);
                    // stack : ..., obj, double value, obj
                } else {
                    // stack : ..., obj, single value
                    mv.visitInsn(Opcodes.DUP2);
                    // stack : ..., obj, single value, obj, single value
                    mv.visitInsn(Opcodes.POP);
                    // stack : ..., obj, single value, obj
                }
                break;
            case Opcodes.GETSTATIC:
            case Opcodes.PUTSTATIC:
                // nothing to push
                break;
            default:
                throw new IllegalArgumentException("Unexpected opcode: " + Integer.toHexString(opCode));
        }
    }
}
