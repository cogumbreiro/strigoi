# Strigoi: Dynamic ghost-state instrumentation for Java.

Strigoi is a Java library that can instrument fields of classes.

Example 1, invoke a static method whenever a field is accessed:

```java
public class Main {
    public static accessField(boolean isPut) {
        System.out.println("Field x was accessed : " + (isPut ? "put" : "get"));
    }
    public static ClassLoader instrument() {
        ClassLoader cl = InstrumentFieldAccess.make()
            // This method is invoked when a field is accessed
            .onAccess(Main.class, "accessField", ParameterType.IS_PUT)
            // build a class loader that will only instrument class Foo
            .buildClassLoader("Foo"::equals);
        Object obj = cl.loadClass("Foo").newInstance();
    }
}
```

Example 2, add a ghost field and invoke a static method whenever a field is accessed:

```java
public class Main {
    public static void accessField(boolean isPut, AtomicInteger c) {
        System.out.println("Field x was accessed " + c.incrementAndGet() + " times: " + (isPut ? "put" : "get"));
    }
    public static AtomicInteger initField() {
        return new AtomicInteger();
    }
    public static ClassLoader instrument() {
        ClassLoader cl = InstrumentFieldStateAccess
            // for each field, create an atomic integer
            .make(AtomicInteger.class)
            // The factory for the auxiliar state
            .onInit(Main.class, "initField")
            // Whenever the field is accessed, this method is called
            .onAccess(Main.class, "accessField", ParameterType.IS_PUT, ParemeterType.GHOST_FIELD)
            // build a class loader that will only instrument class Foo
            .buildClassLoader("Foo"::equals);
        Object obj = cl.loadClass("Foo").newInstance();
    }
}
```

Example 3, 
```java
public class Main {
    public static void accessArray(Object array, int index, boolean isPut) {
        System.out.println("Field x was accessed " + c.incrementAndGet() + " times: " + (isPut ? "put" : "get"));
    }
    public static void initArray(Object array) {
        System.out.println("Array initialized!");
    }
    public static ClassLoader instrument() {
        ClassLoader cl = InstrumentArrayAccess.make()
            // This method is invoked after an array is created
            .onInit(Main.class, "initArray")
            // This method is invoked whenever an array is accessed
            .onAccess(Main.class, "accessArray", ParameterType.IS_PUT)
            // build a class loader that will only instrument class Foo
            .buildClassLoader("Foo"::equals);
        Object obj = cl.loadClass("Foo").newInstance();
    }
}
```
